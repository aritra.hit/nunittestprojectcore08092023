using NUnit.Framework;
using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Remote;
using System.Threading;

namespace nunittestprojectcore08092023
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void Test1()
        {
            var chromeOptions = new ChromeOptions();

            chromeOptions.AddArgument("--headless");
            chromeOptions.AddArgument("--no-sandbox");

            IWebDriver driver = new ChromeDriver(chromeOptions);
            driver.Url = "http://www.google.com";
            Thread.Sleep(5000);
            Console.WriteLine("Title returned : " + driver.Title);
            driver.Close();
            driver.Quit();
            Assert.IsTrue(true);
        }
    }
}